Münchner Physik-Kolloquium
==========================

Organisation des Münchner Physik-Kolloquiums 

Die Vorträge für das kommende Semester finden sich in der entsprechenden Datei 
für das Semester. 

**Achtung: Nach dem Editieren muss unten auf das grüne "Commit changes" gedrückt werden, um die Datei zu speichern!!!**

Aktuell ist: 
[kolloquium-2024-Sommer.txt](https://gitlab.lrz.de/jowi/kolloquium/blob/programmplanung/kolloquium-2024-Sommer.txt)

Die Felder sind in der Mustervorlage erklärt: 
[kolloquium-vorlage.txt](https://gitlab.lrz.de/jowi/kolloquium/blob/programmplanung/kolloquium-vorlage.txt)

In der Datei [checkdatei.txt](https://gitlab.lrz.de/jowi/kolloquium/-/blob/programmplanung/checkdatei.txt)
finden sich aktuelle Informationen zu Kolloquium, wie sie auch veröffentlicht werden. 

Das fertige Programm findet sich unter: https://www.nat.tum.de/nat/aktuelles/veranstaltungen/physik-kolloquium/

Vom `gitlab` am LRZ holen: 

    git clone git+ssh://git@gitlab.lrz.de/jowi/kolloquium.git <local-folder>
    cd <local-folder>
    git checkout -b ss-2022 origin/ss-2022

