:Date:         2024-04-15
:Title:        One hundred years of electrified interfaces: What’s new with the theories of Debye and Onsager?
:Speaker:      Prof. Dr. David Andelmann
:Gender:       male
:Affiliation:  School of Physics, Tel Aviv University, Israel
:Location:     LMU
:Meet-speaker: none
:Abstract: 
The Poisson-Boltzmann theory stems from the pioneering works of Debye and
Onsager and is considered even today as the benchmark of ionic solutions and
electrified interfaces. It has been instrumental during the last century in
predicting charge distributions and interactions between charged surfaces, 
membranes, electrodes, macromolecules, and colloids. The electrostatic model
of charged fluids, on which the Poisson-Boltzmann description rests and its
statistical mechanical consequences have been scrutinized in great detail.
Much less, however, is understood about its probable shortcomings when 
dealing with various aspects of real physical, chemical, and biological 
systems. After reviewing the Poisson-Boltzmann theory, I will discuss
several extensions and modifications to the seminal works of Debye and
Onsager as applied to ions and macromolecules in confined geometries. These
novel ideas include the effect of dipolar solvent molecules, finite size of
ions, ionic specificity, surface tension, and conductivity of concentrated 
ionic solutions.
:Endabstract:

:Date:         2024-06-03
:Title:        Gedenkkolloquium Prof. Dr. Wolfgang Kaiser
:Gender:       male
:Location:     TUM
:Meet-speaker: none     
:Abstract: 
1.) R. Kienberger, TUM: 		Allgemeine Begrüßung, Motivation des Symposiums.

2.) Präsident Th. Hofmann, TUM: 	grobe Übersicht Leben Kaiser, speziell Berufung an TUM und Ehrungen.

3.) Dekan J. Barth, TUM: 		Kaisers Bedeutung für das (junge) Physik-Department.

4.) W. Zinth, LMU: 			“Wolfgang Kaiser´s scientific achievements”

5.) E. Ippen, MIT, Boston, USA: 		“Development of Ultrafast Science”
:Endabstract:

:Date:         2024-06-10
:Title:        Science Education in an international Context -- Research and Education in Modern Science
:Speaker:      Dr. Sascha Marc Schmeling
:Gender:       male
:Affiliation:  CERN, Genf, Schweitz
:Location:     LMU
:Meet-speaker: none     
:Abstract:
CERN, one of the oldest European intergovernmental organisations, celebrates
its 70th anniversary in 2024. The foundational CERN Convention was finalized
in the summer of 1953, setting out the contributions of its Member States and
its dedication to the dissemination of research results, international
peaceful collaboration, and the education of future scientists.

The discussion will centre on the integration of scientific research and
education within an international context, emphasising the crucial role of
international partnerships in enriching science education. A key focal point
will be CERN's Science Gateway, a pioneering educational project designed to
make science accessible to a diverse audience. We will also delve into the
implications of artificial intelligence in science education, exploring both
its potential to transform traditional learning environments and its
integration into cutting-edge scientific research. Through examples of
successful collaborations and innovative educational initiatives, I will 
demonstrate how merging research with education can cultivate a globally 
informed, scientifically adept society, equipped to tackle the complexities
of the future.

:Endabstract:

:Date:         2024-06-17
:Title:        Searching for dark matter deep underground -- ongoing puzzle and what we have found so far
:Speaker:      Dr. Karoline Schäffner
:Gender:       female
:Affiliation:  Max-Planck Institut für Physik, Garching
:Location:     TUM
:Meet-speaker: none     
:Abstract: 
The quest for the nature of dark matter (DM) is one of today’s largest secrets
in cosmology and astroparticle physics and the fast-growing science community
impatiently awaits its discovery. 
DM makes up more than 80% of the matter in our universe and the Standard Model
of particle physics, though very successful in explaining the visible part
thereof, lacks an explanation for it. 

Direct detection experiments are the crucial avenue to investigate the nature
of DM particles by measuring the nuclear recoil energy induced by their elastic
scattering off the detector’s target nuclei. 
Even more, since the well-motivated and most prominent DM candidate, the Weakly
Interacting Massive Particle (WIMP), is eluding discovery, direct detection is
the most promising path for the assessment of many theoretical models. 
Searching for a direct DM signal above the detector background includes looking
for an annual modulation signal caused by the seasonal variation of the Earth’s
velocity with respect to the sun and, thus, the dark matter halo. The
DAMA/LIBRA experiment, a pioneer using such modulation as DM signature,
observes a modulated signal rate with a very high statistical significance, and
period and phase matching the DM expectation. Yet, so far, this modulation
signal could neither be confirmed by any other DM search nor explained to be of
non-DM origin. A model-independent cross-check by another detector also using
sodium iodide crystals is still missing to unveil this long-term puzzle.

In this colloquium, I will talk about the present status of direct dark matter
searches, with emphasis on the low-mass dark matter search with cryogenic 
detectors and COSINUS, a new player in the dark matter community working on 
shedding light on the DAMA mystery and close with an outlook to future
perspectives. 
:Endabstract:

:Date:         2024-06-24
:Title:        
:Speaker:      
:Gender:       male
:Affiliation:  
:Location:     LMU
:Meet-speaker: none     
:Abstract: 
:Endabstract:

:Date:         2024-07-01
:Title:        
:Speaker:      
:Gender:       male
:Affiliation:  
:Location:     TUM
:Meet-speaker: none     
:Abstract: 
:Endabstract:

:Date:         2024-07-08
:Title:        
:Speaker:      
:Gender:       male
:Affiliation:  
:Location:     LMU
:Meet-speaker: none     
:Abstract: 
:Endabstract:

:Date:         2024-07-15
:Title:        
:Speaker:      
:Gender:       male
:Affiliation:  
:Location:     TUM
:Meet-speaker: none     
:Abstract: 
:Endabstract:

:Date:         2024-07-22
:Title:        
:Speaker:      
:Gender:       male
:Affiliation:  
:Location:     LMU
:Meet-speaker: none     
:Abstract: 
:Endabstract:

:Date:         2024-07-29
:Title:        
:Speaker:      
:Gender:       male
:Affiliation:  
:Location:     TUM
:Meet-speaker: none     
:Abstract: 
:Endabstract:

:Date:         2024-08-05
:Title:        
:Speaker:      
:Gender:       male
:Affiliation:  
:Location:     LMU
:Meet-speaker: none     
:Abstract: 
:Endabstract:
